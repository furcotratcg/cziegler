/* eslint-disable jsx-a11y/img-redundant-alt */
import { ImageCard } from "../../common/imageCard";
import { IPost } from "../types";

export let __className__: IPost = {
    'id': __id__,
    'title': '__title__',
    'date': new Date('__yyyy__/__mm__/__dd__'),
    'tags': ['Journal'],
    'body':
<>
<div>
    <p>
        Reading: 
        <br></br>
        Playing: 
        <br></br>
        Mood: 
    </p>
    <div style={{'display':'flex'}}>
        <div style={{'flexGrow': 3}}>
            <p>
                Put things here
            </p>
            <p>
                Song of the day
                <br></br> 
                <iframe  height="200" src="https://www.youtube.com/embed/TSzp6-SV5FI?si=Es3ot7CJp6Ouqu4a" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowFullScreen></iframe>                
                <blockquote>
                    Lyrics lyrics<br></br>
                    Lyrics lyrics<br></br>
                    Lyrics lyrics<br></br>
                    Lyrics lyrics<br></br>
                </blockquote>
            </p>
        </div>
        <ImageCard 
            url="/assets/images/IMAGE.jpg" 
            altText="ALTTEXT"
            caption="CAPTION"/>
    </div>
</div>
</>
}