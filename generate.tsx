import { CaseConverterEnum, generateTemplateFiles } from 'generate-template-files';
import { Posts } from './src/blog/posts'
import fs from 'fs';

const today = new Date()
const month = today.toLocaleString('default', { month: 'short' });
const year = today.getFullYear();
const mm = today.getMonth() + 1;
const dd = today.getDate();
const yy = today.getFullYear() % 2000

generateTemplateFiles([
  {
    defaultCase: CaseConverterEnum.None,
    option: 'Create Blog Post',
    entry: {
      folderPath: './templates/blog.tsx',
    },
    stringReplacers: ['__title__'],
    dynamicReplacers: [
        {slot: '__className__', slotValue: `${month}${dd}${yy}`,},
        {slot: '__yyyy__', slotValue: year + ''},
        {slot: '__dd__', slotValue: dd + ''},
        {slot: '__mm__', slotValue: mm + ''},
        {slot: '__id__', slotValue: (Posts.length + 1) + ''}
    ],
    output: {
      path: `./src/blog/${month.toLowerCase()}${year}/${mm}-${dd}-${yy}.tsx`,
      pathAndFileNameDefaultCase: CaseConverterEnum.None,
      overwrite: false,
    },
    onComplete: (results) => {
      let data = fs.readFileSync('./src/blog/posts.tsx', 'utf-8');
      let newData = data.replace('IPost[] = [\n', `IPost[] = [\n    ${`${month}${dd}${yy}`},\n`)
      newData = newData.replace(/^/, `import { ${`${month}${dd}${yy}`} } from "./${month.toLowerCase()}${year}/${mm}-${dd}-${yy}"\n`)

      fs.writeFileSync('./src/blog/posts.tsx', newData, 'utf-8')
      console.log(results);
    },
  }])