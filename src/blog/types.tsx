import { BigIntStats } from "fs"
import { ReactElement } from "react"

export interface IPost {
    id: number,
    title: string,
    date: Date,
    tags: string[],
    body: ReactElement
}