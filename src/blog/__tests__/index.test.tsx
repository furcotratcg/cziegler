import React from "react";
import { render, screen } from "@testing-library/react";
import { Blog } from "..";
import { IPost } from "../types";

const posts: IPost[] = [
    {
        'id': 0,
        'title': 'Hello World',
        'date': new Date('2023/11/13'),
        'tags': ['Journal', 'Art'],
        'body': <><div>Hi</div></>
    },
    {
        'id': 1,
        'title': 'Hello The Sequel',
        'date': new Date('2023/11/14'),
        'tags': ['Journal'],
        'body': <><div>Hello again</div></>
    }
];

test("set names are there", () => {

});
