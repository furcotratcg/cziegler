import { useFabric } from "./fabric";
import { Canvas as CanvasType } from 'fabric/fabric-impl';


export function Canvas(props: {
  canvasHook: (canvas: CanvasType) => void;
  width: number;
  height: number;
}) {
  const ref = useFabric((fabricCanvas: CanvasType) => {
    props.canvasHook(fabricCanvas);
  });
  return (
    <>
      <div style={{ display: "inline-block" }}>
        <canvas
          style={{ borderColor: "black", border: "solid" }}
          ref={ref}
          width={props.width}
          height={props.height}
        />
      </div>
    </>
  );
}
